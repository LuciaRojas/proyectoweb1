<?php

include 'conexion.php';

class Functions{

/***
 * trae los datos de los arboles con la fehca especificada
 */
function treemes(){
  $conn= new Conexion();

  $sql = "SELECT *, DATE_FORMAT(fechaedit, '%y-%m-%d') FROM tree WHERE fechaedit BETWEEN SYSDATE() - INTERVAL 30 DAY AND SYSDATE() ORDER BY id_tree DESC";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;

}
/***
 * Elimina la fotografia en la base de datos por el id de la fotografia 
 * $id_photografi = id de la fotografia 
 */


function deletphoto($id_photografi){
  $conn= new Conexion();

  $sql = "DELETE FROM `photographs` WHERE `id_Photography`  = $id_photografi";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;

}


/**
 * Inserts a new student to the database
 *
 * @student An associative array with the student information
 */
function saveStudent($student) {
  $conn= new Conexion();
 
  $sql = "INSERT INTO students( `full_name`, `email`, `document`)
          VALUES ('{$student['full_name']}', '{$student['email']}', '')";
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

/**
 * Inserta los datos del usuario a la hora de registrarse
 * $username los datos del usuario
 */

function saveUsernamet($username) {
  $conn= new Conexion();

  $sql = "INSERT INTO users( `name`, `last_name`, `num_tel`, `email`, `address`, `country`, `user`, `password`,`user_type`)
          VALUES ('{$username['name']}',  '{$username['last_name']}','{$username['num_tel']}','{$username['email']}','{$username['address']}','{$username['country']}','{$username['username']}','{$username['password']}','friend')";
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}
function saveTree($tree) {
  $conn= new Conexion();

  $sql = "INSERT INTO tree(`id_user`, `nom_tree`, `id_especie`, `monto`,`fecha`)
          VALUES ('{$tree['id_user']}',  '{$tree['name']}','{$tree['id_especie']}','{$tree['monto']}','{$tree['fecha']}')";
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}



/**
 * Obtiene los datos de usuario que son friend
 */
function getAmigos(){
  $conn= new Conexion();

  $sql = "SELECT * FROM users WHERE `user_type` = 'friend'";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}
/**
 * Obtiene los datos de usuario que son Admin
 */
function getAdmin(){
  $conn= new Conexion();

  $sql = "SELECT * FROM users WHERE `user_type` = 'Admin'";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}
/**
 * Obtiene los datos del arbol
 */

function getArbol(){
  $conn= new Conexion();

  $sql = "SELECT * FROM tree";
  $result = $conn->query($sql);


  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 * Obtiene los datos de de las fotografias mediante el id del arbol
 * $id_tree el id del árbol
 */
function getphotografy( $id_tree){
  $conn= new Conexion();

  $sql = "SELECT * FROM `photographs` WHERE `id_tree` = $id_tree  ORDER BY `id_Photography` DESC";
  $result = $conn->query($sql);


  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 * Obtiene los datos de los árboles mediante el id del árbol
 * $id_tree el id del árbol
 */
function getTreeUser($id_tree){
  $conn= new Conexion();
  $sql = "SELECT * FROM tree WHERE id_tree= $id_tree";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result->fetch_array();
}

/**
 * Obtiene los datos de las especies
 */
function getEspecies(){
  $conn= new Conexion();
  $sql = "SELECT * FROM especie";
  $result = $conn->query($sql);
  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 * Actualiza la infromacion del árbol
 * $tree datos del arbol a actualizar
 */
function updateTree($tree) {
  $conn= new Conexion();
  $sql = "UPDATE tree set `alt` = '{$tree['alt']}' , `id_especie` = '{$tree['id_especie']}',`fechaedit` = '{$tree['fechaedit']}' WHERE `id_tree` = {$tree['id_tree']}";

  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

/**
 * Obtiene los datos de los arboles mediante el id del arbol
  * $id_tree el id del árbol
 */
function getTree($id_tree){
  $conn= new Conexion();
  $sql = "SELECT e.especie,t.id_tree, t.nom_tree,t.alt  FROM users u INNER JOIN tree t ON u.id_user=t.id_user Inner Join especie e On e.id_especie = t.id_especie where u.id_user= $id_tree";
  $result = $conn->query($sql);


  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}
/**
 * Obtiene los datos de los arboles del usuario mediante el id del usuario 
  * $id_usuario el id del user
 */
function getUserTree($id_user){
  $conn= new Conexion();
  $sql = "SELECT u.id_user,t.id_tree,t.nom_tree,e.id_especie,e.especie,t.monto FROM users u INNER JOIN tree t ON u.id_user=t.id_user Inner Join especie e On e.id_especie = t.id_especie where u.id_user=$id_user";
  $result = $conn->query($sql);


  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}





/**
 * Get one specific student from the database
 *
 * @id Id of the student
 */
function authenticate($email, $password){
  $conn= new Conexion();
  $sql = "SELECT * FROM users WHERE `email` = '$email' AND `password` = '$password'";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return $result->fetch_array();
}

/**
 * Get one specific student from the database
 *
 * @id Id of the student
 */
function authenticatePhotogra($name){
  $conn= new Conexion();
  $sql = "SELECT * FROM `photographs` WHERE `profilePic` = '$name'";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return $result->fetch_array();
}
/**
 * valida que el email se encuentre en la base de datos
 *
 * @email email del usuario
 */

function validaremail($email){
  $conn= new Conexion();
  $sql = "SELECT * FROM users WHERE `email` = '$email'";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return $result->fetch_array();
}

/**
 * Deletes an student from the database
 */
function deleteStudent($id){
  $conn= new Conexion();
  $sql = "DELETE FROM students WHERE id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

/**
 * Uploads an image to the server
 *
 * @inputName name of the input that holds the image in the request
 */

function uploadPicture($inputName,$id_tree){
  date_default_timezone_set('America/Costa_Rica');
  $fecha= date("yy/m/d"); 
  $fileObject = $_FILES[$inputName];

  $target_dir = "uploads/";
  $target_file = $target_dir . basename($fileObject["name"]);
  $uploadOk = 0;
  if (move_uploaded_file($fileObject["tmp_name"], $target_file)) {


    $conn= new Conexion();
    $sql = "INSERT INTO photographs(`id_tree`, `profilePic`,`fechap`)
           VALUES ('$id_tree','$target_file','$fecha')";
     $conn->query($sql);
     if ($conn->connect_errno) {
      $conn->close();
    }
    $conn->close();

    return $target_file;
  } else {
    return false;
  }
}




/*
function uploadPicture($inputName,$id_tree){

//Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
foreach($_FILES["picture"]['tmp_name'] as $key => $tmp_name)
{
  //Validamos que el archivo exista
  if($_FILES["picture"]["name"][$key]) {
    $filename = $_FILES["picture"]["name"][$key]; //Obtenemos el nombre original del archivo
    $source = $_FILES["picture"]["tmp_name"][$key]; //Obtenemos un nombre temporal del archivo
    
    $directorio = 'uploads'; //Declaramos un  variable con la ruta donde guardaremos los archivos

/***************************************************************************** */
    
/***************************************************************************** */

 //Validamos si la ruta de destino existe, en caso de no existir la creamos
   
 /*if(!file_exists($directorio)){
      mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");	
    }

    $dir=opendir($directorio); //Abrimos el directorio de destino
    $target_path = $directorio.'/'.$filename; //Indicamos la ruta de destino, así como el nombre del archivo
  

    $conn= new Conexion();
    $sql = "SELECT * FROM `photographs` WHERE `profilePic` = '$target_path'";
    $result = $conn->query($sql);
  
    if ($conn->connect_errno) {
      $conn->close();
    }
    $conn->close();
     $result->fetch_array();

     
     }
     date_default_timezone_set('America/Costa_Rica');
     $fecha= date("yy/m/d"); 
    //Movemos y validamos que el archivo se haya cargado correctamente
    //El primer campo es el origen y el segundo el destino
   

    if(move_uploaded_file($source, $target_path)) {	
      echo "El archivo $filename se ha almacenado en forma exitosa.<br>";
  $conn= new Conexion();
      $sql = "INSERT INTO photographs(`id_tree`, `profilePic`,`fechap`)
             VALUES ('$id_tree','$target_path','$fecha')";
       $conn->query($sql);
       if ($conn->connect_errno) {
        $conn->close();
      }
      $conn->close();
    closedir($dir); //Cerramos el directorio de destino*/
   
   /*}
   else{
    return false;
   }
  }
  return true;
}*/



}
