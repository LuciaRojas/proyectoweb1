<?php
       include 'functions.php';

       session_start();
       $user = $_SESSION['user'];
       /**si el usuario no es amigo lo saca de la pagina */
       if (!$user|| $user['user_type'] =="Admin") {
         echo $user['ebtri'];
         header('Location: ../index.php');
       }



  $Clfuncion= new Functions();
  $message = "";
   
  $id_tree=$_REQUEST['id_user'];
  $id_tree=$user['id_user'];



if($_GET){

$user['id_user']=$_GET['id_user'];
}
if($_POST){

  $user['id_user']=$_POST['id_user'];
  }


  if(!empty($_REQUEST['status'])) {

    switch($_REQUEST['status']) {
      case 'success':
        $message = 'Tree was added succesfully';
      break;
      case 'error':
        $message = 'There was a problem inserting the tree';
      break;
    }
  }

 
  date_default_timezone_set('America/Costa_Rica');
      $fecha= date("yy/m/d"); 
  
?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="../css/style.css">
     <script type="text/javascript" src="../js/funciones.js"></script>
     <title>Document</title>
 </head>
 <body>
 <div class="msg">
      <?php echo $message; ?>
    </div>
    <div class="contenedor">
        <h2>Insert Tree</h2>
      
        <form  action="createtree.php" method="POST" class="form-inline" role="form" enctype="multipart/form-data">
        <input type="hidden" name="id_user" value="<?php echo $id_tree?>">
        <input type="hidden" name="fecha" value="<?php echo $fecha?>">
        <div class="form-group">
        <input type="hidden" name="id" >
        </div>
        <div class="form-group">
        <input type="text"  id="" name="name" placeholder="Nombre"  class="input__text" required >
        <input type="number"  id="" name="monto" placeholder="Monto a donar"  class="input__text" require onkeypress=" return SoloNumeros(event);">
        </div>
        <div>
        <select  id="id_especie" name="id_especie" class="input__text"  require>
        <option value="" disabled selected >Select Especie</option>
        <?php
          $especies =$Clfuncion-> getEspecies();
          $especiesHtml = "";
          foreach ($especies as $especie) {
            if($especie['id_especie'] == $tree['id_especie']){
              $especiesHtml .= "<option selected='true' name='{$especie['id_especie']}' value={$especie['id_especie']}> {$especie['especie']}</option>";
            }else{
              $especiesHtml .= "<option name='{$especie['id_especie']}' value={$especie['id_especie']}> {$especie['especie']} </option>";
            }
          }
          echo $especiesHtml;
         
        ?>
        </select>

        </div>
        <div class="form-group">
        </div >

        <div class="form-group">
        <button type="submit" class="btn__primary">Save</button>
      
        </div class="form-group">
        <div>
        <a href="prinfriend.php"  class="btn__danger">Volver</a>
        </div>
        </form>
        
    </div>
 </body>
 </html>