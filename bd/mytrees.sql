-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-03-2020 a las 06:54:04
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mytrees`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especie`
--

CREATE TABLE `especie` (
  `id_especie` int(11) NOT NULL,
  `especie` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `especie`
--

INSERT INTO `especie` (`id_especie`, `especie`) VALUES
(1, 'Cortés amarillo'),
(2, 'Llama del Bosque'),
(3, 'Roble Sabana'),
(4, 'Guanacaste '),
(5, 'Guachipelín');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `photographs`
--

CREATE TABLE `photographs` (
  `id_Photography` int(11) NOT NULL,
  `id_tree` int(11) NOT NULL,
  `profilePic` varchar(500) NOT NULL,
  `fechap` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tree`
--

CREATE TABLE `tree` (
  `id_tree` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nom_tree` varchar(200) NOT NULL,
  `alt` float NOT NULL,
  `id_especie` varchar(200) NOT NULL,
  `monto` float NOT NULL,
  `fecha` date NOT NULL,
  `fechaedit` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tree`
--

INSERT INTO `tree` (`id_tree`, `id_user`, `nom_tree`, `alt`, `id_especie`, `monto`, `fecha`, `fechaedit`) VALUES
(13, 4, 'Juancito', 15, '1', 5000, '2020-03-09', '2020-03-09'),
(14, 4, 'meo', 0, '2', 9000, '2020-03-09', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `num_tel` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `country` varchar(200) NOT NULL,
  `user` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `user_type` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id_user`, `name`, `last_name`, `num_tel`, `email`, `address`, `country`, `user`, `password`, `user_type`) VALUES
(1, 'Lucia Rojas', 'Rojas', 89708134, 'luciarojas525@gmail.com', 'Ciudad Quesada, Barrio San Antonio ,frente encomienda Lázaro', 'Barrio San Antonio', 'Lucia', '2525', 'Admin'),
(2, 'Lucia Rojas', 'Rojas', 89708134, 'luciarojas525@gmail.com', 'Ciudad Quesada, Barrio San Antonio ,frente encomienda Lázaro', 'Costa Rica', 'Lucia', '147', 'friend'),
(4, 'Xinia', 'Campos', 24604631, 'Xiniacc1@gmail.com', 'Barrio san antonio', 'Costa Rica', 'xinia1', '789', 'friend'),
(5, 'Eduardo', 'Rojas', 89708134, 'eduar@hotmail.com', 'Ciudad Quesada, Barrio San Antonio ,frente encomienda Lázaro', 'Costa Rica', 'edaur20', '456', 'friend');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `especie`
--
ALTER TABLE `especie`
  ADD PRIMARY KEY (`id_especie`);

--
-- Indices de la tabla `photographs`
--
ALTER TABLE `photographs`
  ADD PRIMARY KEY (`id_Photography`) USING BTREE;

--
-- Indices de la tabla `tree`
--
ALTER TABLE `tree`
  ADD PRIMARY KEY (`id_tree`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `especie`
--
ALTER TABLE `especie`
  MODIFY `id_especie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `photographs`
--
ALTER TABLE `photographs`
  MODIFY `id_Photography` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `tree`
--
ALTER TABLE `tree`
  MODIFY `id_tree` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
